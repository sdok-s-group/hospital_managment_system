<!-- field_type_name -->

@include('crud::fields.inc.wrapper_start')

@if (isset($entry->units))
    @foreach ($entry->units as $item)
    {{-- {{dd($item)}} --}}
    <div id="building">
        <div class="card building-info">
            <div class="col-md-12 building-wrapper hidden-building-form mt-3">
                    <div class="building-content z-depth-1-half rounded p-3 mb-3">
                        <div class="row">
                            <div class="d-flex justify-content-between col-md-12">
                                <div class="title-left">
                                    <h5 class="building-title">{!! $field['label'] !!} <span class="building-id"></span></h5>
                                </div>
                                <div class="button-right">
                                    <button type="button" class="close remove-building" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                        <nav class="navbar navbar-light bg-light mt-3">
                                        <span class="navbar-brand mb-0 h4">Basic Information</span>
                                        </nav>
                                        </div>

                                    @include('crud::fields.hidden', [
                                        "field" => [
                                        'name'  => 'unit_id',
                                            'wrapperAttributes' => [
                                            'class' => 'form-group col-md-12'
                                            ],
                                            'value' => $item->id,
                                    ]])

                                    @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "parent_project",
                                        "label" => 'Parents Project',
                                        "type"=> "select2_from_array",
                                        'options'     => [
                                                            '-'=>'-',
                                                            'Borey Peng Hout' => 'Borey Peng Hout',
                                                            'Borey Leng Navatra' => 'Borey Leng Navatra'
                                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-12'],
                                        'value' => $item->parent_project,
                                    ]])

                                    @include('crud::fields.text', [
                                        "field" => [
                                        "name" => "project_name",
                                        "label" => 'Project Name',
                                        "type"=> "text",
                                        'wrapper' => ['class' => 'form-group col-md-12'],
                                        'value' => $item->project_name,
                                    ]])

                                        @include('crud::fields.text', [
                                            "field" => [
                                            "name" => "name",
                                            "label" => 'Building Name',
                                            "type"=> "text",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->name,
                                        ]])

                                        @include('crud::fields.select2_from_array', [
                                            "field" => [
                                            "name" => "style",
                                            "label" => 'Style',
                                            "type"=> "select2_from_array",
                                            'options'     => ['modern' => 'Modern','classic' => 'Classic'],
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->style,

                                        ]])

                                        @include('crud::fields.select2_from_array', [
                                            "field" => [
                                            "name" => "current_use",
                                            "label" => 'Current Use',
                                            "type"=> "select2_from_array",
                                            'options'     => [
                                                                '-'=>'-',
                                                                'Improved Land' => 'Improved Land',
                                                                'Non-Improved Land' => 'Non-Improved Land',
                                                                'Farms' => 'Farms',
                                                                'Ranches' => 'Ranches',
                                                                'Orchards' => 'Orchards',
                                                                'Timberland' => 'Timberland',
                                                                'Condominium' => 'Condominium',
                                                                'Apartment' => 'Apartment',
                                                                'Co-Ownership Property' => 'Co-Ownership Property',
                                                            ],
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->current_use,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "width",
                                            "label" => 'Width',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->width,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "length",
                                            "label" => 'Length',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->length,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "area",
                                            "label" => 'Total Size',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->area,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "groos_floor_area",
                                            "label" => 'Gross Floor Area (GFA)',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12']
                                            ,
                                            'value' => $item->groos_floor_area,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "net_floor_area",
                                            "label" => 'Net Floor Area',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->net_floor_area,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "bedroom",
                                            "label" => '# of Bedroom',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->bedroom,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "bathroom",
                                            "label" => '# of Bathroom',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->bathroom,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "livingroom",
                                            "label" => '# of Living Room',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->livingroom,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "diningroom",
                                            "label" => '# of Dinning Room',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->diningroom,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "floor",
                                            "label" => '# of Floor',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->floor,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "stories",
                                            "label" => '# of Storey',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->stories,
                                        ]])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                        <nav class="navbar navbar-light bg-light mt-3">
                                        <span class="navbar-brand mb-0 h4">Features</span>
                                        </nav>
                                        </div>

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "car_parking",
                                            "label" => 'Car Parking',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->car_parking,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "motor_parking",
                                            "label" => 'Motor Parking',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->motor_parking,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "swimming_pool",
                                            "label" => 'Swimming Pool',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->swimming_pool,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "fitness_gym",
                                            "label" => 'Fitness Gym',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->fitness_gym,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "lift",
                                            "label" => 'Lift',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->lift,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "balcony",
                                            "label" => 'Balcony',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->balcony,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "kitchen",
                                            "label" => 'Kitchen',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->kitchen,
                                        ]])

                                        @include('crud::fields.checkbox', [
                                            "field" => [
                                            "name" => "security",
                                            "label" => 'Security Guard',
                                            "type"=> "checkbox",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->security,
                                        ]])

                                        <div class="form-group col-md-12">
                                        <nav class="navbar navbar-light bg-light mt-3">
                                        <span class="navbar-brand mb-0 h4">Other</span>
                                        </nav>
                                        </div>

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "cost_estimate",
                                            "label" => 'Cost Estimate',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->cost_estimate,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "useful_life",
                                            "label" => 'Useful Life',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->useful_life,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "effective_age",
                                            "label" => 'Effective Age',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->effective_age,
                                        ]])

                                        @include('crud::fields.number', [
                                            "field" => [
                                            "name" => "completion_year",
                                            "label" => 'Completion Year',
                                            "type"=> "number",
                                            'wrapper' => ['class' => 'form-group col-md-12'],
                                            'value' => $item->completion_year,
                                        ]])

                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    @endforeach

@else


<div id="building">
    <div class="card building-info">
        <div class="col-md-12 building-wrapper hidden-building-form mt-3">
                <div class="building-content z-depth-1-half rounded p-3 mb-3">
                    <div class="row">
                        <div class="d-flex justify-content-between col-md-12">
                            <div class="title-left">
                                <h5 class="building-title">{!! $field['label'] !!} <span class="building-id"></span></h5>
                            </div>
                            <div class="button-right">
                                <button type="button" class="close remove-building" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                    <nav class="navbar navbar-light bg-light mt-3">
                                    <span class="navbar-brand mb-0 h4">Basic Information</span>
                                    </nav>
                                    </div>

                                    <div class="hidden">
                                    <input type="hidden" name="hidden_id" value="" class="form-control">
                                    </div>


                                    @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "parent_project",
                                        "label" => 'Parents Project',
                                        "type"=> "select2_from_array",
                                        'options'     => [
                                                            '-'=>'-',
                                                            'Borey Peng Hout' => 'Borey Peng Hout',
                                                            'Borey Leng Navatra' => 'Borey Leng Navatra'
                                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.text', [
                                        "field" => [
                                        "name" => "project_name",
                                        "label" => 'Project Name',
                                        "type"=> "text",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.text', [
                                        "field" => [
                                        "name" => "name",
                                        "label" => 'Building Name',
                                        "type"=> "text",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "style",
                                        "label" => 'Style',
                                        "type"=> "select2_from_array",
                                        'options'     => [
                                                            '-'=>'-',
                                                            'modern' => 'Modern',
                                                            'classic' => 'Classic'
                                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "current_use",
                                        "label" => 'Current Use',
                                        "type"=> "select2_from_array",
                                        'options'     => [
                                                            '-'=>'-',
                                                            'Improved Land' => 'Improved Land',
                                                            'Non-Improved Land' => 'Non-Improved Land',
                                                            'Farms' => 'Farms',
                                                            'Ranches' => 'Ranches',
                                                            'Orchards' => 'Orchards',
                                                            'Timberland' => 'Timberland',
                                                            'Condominium' => 'Condominium',
                                                            'Apartment' => 'Apartment',
                                                            'Co-Ownership Property' => 'Co-Ownership Property'
                                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "width",
                                        "label" => 'Width',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "length",
                                        "label" => 'Length',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "area",
                                        "label" => 'Total Size',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "groos_floor_area",
                                        "label" => 'Gross Floor Area (GFA)',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "net_floor_area",
                                        "label" => 'Net Floor Area',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "bedroom",
                                        "label" => '# of Bedroom',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "bathroom",
                                        "label" => '# of Bathroom',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "livingroom",
                                        "label" => '# of Living Room',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "diningroom",
                                        "label" => '# of Dinning Room',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "floor",
                                        "label" => '# of Floor',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "stories",
                                        "label" => '# of Storey',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                    <nav class="navbar navbar-light bg-light mt-3">
                                    <span class="navbar-brand mb-0 h4">Features</span>
                                    </nav>
                                    </div>

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "car_parking",
                                        "label" => 'Car Parking',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "motor_parking",
                                        "label" => 'Motor Parking',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "swimming_pool",
                                        "label" => 'Swimming Pool',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "fitness_gym",
                                        "label" => 'Fitness Gym',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "lift",
                                        "label" => 'Lift',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "balcony",
                                        "label" => 'Balcony',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "kitchen",
                                        "label" => 'Kitchen',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.checkbox', [
                                        "field" => [
                                        "name" => "security",
                                        "label" => 'Security Guard',
                                        "type"=> "checkbox",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    <div class="form-group col-md-12">
                                    <nav class="navbar navbar-light bg-light mt-3">
                                    <span class="navbar-brand mb-0 h4">Other</span>
                                    </nav>
                                    </div>

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "cost_estimate",
                                        "label" => 'Cost Estimate',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "useful_life",
                                        "label" => 'Useful Life',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "effective_age",
                                        "label" => 'Effective Age',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                    @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "completion_year",
                                        "label" => 'Completion Year',
                                        "type"=> " number",
                                        'wrapper' => ['class' => 'form-group col-md-12']
                                    ]])

                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endif
{{-- <button id="btnAddBuilding" class="btn btn-primary my-3" type="button"><span class="ladda-label"><i class="la la-plus"></i></span> Add Building</button> --}}
@include('crud::fields.inc.wrapper_end')

@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD EXTRA CSS  --}}
    {{-- push things in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- no styles -->
    @endpush

    {{-- FIELD EXTRA JS --}}
    {{-- push things in the after_scripts section --}}
    {{-- @push('crud_fields_scripts')
    <script type="text/javascript">
        $(function () {
           $('#btnAddBuilding').click(function(){
               $('.building-info:last').clone(true).appendTo('#building');
                $('.building-info:last input[type=hidden]').attr('value','');
           });


           $('.remove-building').click(function(){
               if($('.building-info').length>1){
                $(this).closest(".building-info").remove();
               }

           });
        });
    </script>
    @endpush --}}
@endif
