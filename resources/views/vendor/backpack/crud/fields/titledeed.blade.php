<!-- field_type_name -->
{{-- {{ dd($entry->titledeed[0]->title_deed_type) }} --}}
{{-- value="{{ !empty($entry) ? $title->title_deed_no : old('title_deed_no') }}" --}}
{{-- id="address-{{($field['name'])}}" --}}
@include('crud::fields.inc.wrapper_start')
@if(!empty($entry))
@foreach ($entry->titledeed as $title)
{{-- {{dd($title)}} --}}
<div id="titledeed">
    <div class="card titledeed-info">
        <div class="col-md-12 titledeed-wrapper hidden-titledeed-form mt-3">
             {{-- for btn-delete itme --}}
             <div class="button-right">
                <button type="button" class="close remove-titledeed" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                <div class="titledeed-content z-depth-1-half rounded p-3 mb-3">
                    <div class="col-md-12">
                    <div class="row" >
                            <div class="col-md-12">
                                <div class="row">
                            @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "title_deed_type[]",
                                        "label" => 'Title Deed Type',
                                        "type"=> "select2_from_array",
                                        'options'=> [
                                                        '-'=>'-',
                                                        'Borey Peng Hout' => 'Borey Peng Hout',
                                                        'Borey Leng Navatra' => 'Borey Leng Navatra'
                                                    ],
                                        'wrapper' => ['class' => 'form-group col-md-6'],
                                        'value'=>$title->title_deed_type,

                                    ]])

                            @include('crud::fields.text', [
                                        "field" => [
                                        "name" => "title_deed_no[]",
                                        "label" => 'Title Deed No.',
                                        "type"=> "text",
                                        'wrapper' => ['class' => 'form-group col-md-6'],
                                         'value'=>$title->title_deed_no,

                                    ]])
                            @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "issued_year[]",
                                        "label" => 'Issued Year',
                                        "type"=> "select2_from_array",
                                        'options' => [
                                            '-'=>'-',
                                                        '2020' => '2020',
                                                        '2019' => '2019',
                                                        '2018' => '2018',
                                                        '2017' => '2017',
                                                        '2016' => '2016',
                                                        '2015' => '2015',
                                                    ],
                                        'wrapper' => ['class' => 'form-group col-md-6'],
                                        'value'=>$title->issued_year,
                                    ]])
                            @include('crud::fields.number', [
                                "field" => [
                                "name" => "parcel_no[]",
                                "label" => 'Parcel No.',
                                "type"=> "number",
                                'wrapper' => ['class' => 'form-group col-md-6'],
                                'value'=>$title->parcel_no,

                            ]])
                            @include('crud::fields.number', [
                                "field" => [
                                "name" => "total_by_titleded[]",
                                "label" => 'Total Size By Title Deed',
                                "type"=> "number",
                                'wrapper' => ['class' => 'form-group col-md-6'],

                            ]])
                             @include('crud::fields.hidden', [
                                "field" => [
                                'name'  => 'hidenid[]',
                                'type' => 'hidden',
                                'label' => '',
                                    'wrapperAttributes' => [
                                    'class' => 'form-group col-md-6'
                                ],
                                'value' =>$title->id,
                            ]])
                            @include('crud::fields.custom_html',[
                                "field"=>[
                                    'name'=>'image_title_in_deed',
                                    'type'=>'custom_html',
                                    'value'=>'<div><b>Title Deed Photo</b>(Each photo is limited within 2MB.)</div>',
                                    'wrapper' => ['class' => 'form-group col-md-12'],
                                ]
                            ])
                           @include('crud::fields.image', [
                            "field" => [
                            "name" => "title_deed_photos[]",
                            "label" => '',
                            "type"=> "image",
                            'default' => 'default.png',
                            'prefix'=>'uploads/files/original/',
                            'wrapperAttributes' => ['class'=> 'form-group col-md-3 bp-image-full-preview'],
                            'value'=>$title->title_deed_photos,
                            ]])

                        </div>
                            </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<button id="btnAdddeed" class="btn btn-primary my-3" type="button"><span class="ladda-label"><i class="la la-plus"></i></span> Add Item</button>
 @endforeach
 {{-- Create title deed to database --}}
@else
<div id="titledeed">
    <div class="card titledeed-info">
        <div class="col-md-12 titledeed-wrapper hidden-titledeed-form mt-3">
             {{-- for btn-delete itme --}}
             <div class="button-right">
                <button type="button" class="close remove-titledeed" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
                <div class="titledeed-content z-depth-1-half rounded p-3 mb-3">
                    <div class="col-md-12">
                    <div class="row" >
                            <div class="col-md-12">
                                <div class="row">
                            @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "title_deed_type[]",
                                        "label" => 'Title Deed Type',
                                        "type"=> "select2_from_array",
                                        'options'=> [
                                                        '-'=>'-',
                                                        'Borey Peng Hout' => 'Borey Peng Hout',
                                                        'Borey Leng Navatra' => 'Borey Leng Navatra'
                                                    ],
                                        'wrapper' => ['class' => 'form-group col-md-6'],

                                    ]])
                            @include('crud::fields.number', [
                                        "field" => [
                                        "name" => "title_deed_no[]",
                                        "label" => 'Title Deed No.',
                                        "type"=> "number",
                                        'wrapper' => ['class' => 'form-group col-md-6'],

                                    ]])
                            @include('crud::fields.select2_from_array', [
                                        "field" => [
                                        "name" => "issued_year[]",
                                        "label" => 'Issued Year',
                                        "type"=> "select2_from_array",
                                        'options' => [
                                                        '-'=>'-',
                                                        '2020' => '2020',
                                                        '2019' => '2019',
                                                        '2018' => '2018',
                                                        '2017' => '2017',
                                                        '2016' => '2016',
                                                        '2015' => '2015',
                                                    ],
                                        'wrapper' => ['class' => 'form-group col-md-6'],
                                        // 'value' => ,
                                    ]])
                            @include('crud::fields.number', [
                                "field" => [
                                "name" => "parcel_no[]",
                                "label" => 'Parcel No.',
                                "type"=> "number",
                                'wrapper' => ['class' => 'form-group col-md-6'],

                            ]])
                            @include('crud::fields.number', [
                                "field" => [
                                "name" => "total_by_titleded[]",
                                "label" => 'Total Size By Title Deed',
                                "type"=> "number",
                                'wrapper' => ['class' => 'form-group col-md-6'],

                            ]])
                             @include('crud::fields.hidden', [
                                "field" => [
                                'name'  => 'hiddenid',
                                'label' => '',
                                    'wrapperAttributes' => [
                                    'class' => 'form-group col-md-6'
                                ],
                            ]])
                            @include('crud::fields.custom_html',[
                                "field"=>[
                                    'name'=>'image_title_in_deed',
                                    'type'=>'custom_html',
                                    'value'=>'<div><b>Title Deed Photo</b>(Each photo is limited within 2MB.)</div>',
                                    'wrapper' => ['class' => 'form-group col-md-12'],
                                ]
                            ])
                           @include('crud::fields.image', [
                            "field" => [
                            "name" => "title_deed_photos[]",
                            "label" => '',
                            "type"=> "image",
                            'default' => 'default.png',
                            'prefix'=>'uploads/files/original/',
                            'wrapperAttributes' => ['class'=> 'form-group col-md-3 bp-image-full-preview'],
                            ]])
                        </div>
                            </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<button id="btnAdddeed" class="btn btn-primary my-3" type="button"><span class="ladda-label"><i class="la la-plus"></i></span> Add Item</button>
@endif
    {{-- HINT
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif --}}
@include('crud::fields.inc.wrapper_end')
{{-- script for repeatable --}}
@push('crud_fields_scripts')
    <script type="text/javascript">
        $(function () {
           $('#btnAdddeed').click(function(){
               $('.titledeed-info:last').clone(true).appendTo('#titledeed');
                $('.titledeed-info:last input[type=hidden]').attr('value','');
           });
           $('.remove-titledeed').click(function(){
               if($('.titledeed-info').length>1){
                $(this).closest(".titledeed-info").remove();
               }

           });
        });
    </script>
    @endpush

