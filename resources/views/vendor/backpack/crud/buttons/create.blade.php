@if ($crud->hasAccess('create'))
    @if(request()->vip)
	    <a href="{{ url($crud->route.'/create?vip=true') }}" class="btn btn-primary text-white" data-style="zoom-in"><span class="ladda-label"><i class="la la-plus"></i>{{ $crud->entity_name }}</span></a>
    @else
        <a href="{{ url($crud->route.'/create') }}" class="btn btn-primary text-white" data-style="zoom-in"><span class="ladda-label"><i class="la la-plus"></i>{{ $crud->entity_name }}</span></a>
    @endif
@endif
