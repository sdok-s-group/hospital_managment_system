<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('department') }}'><i class='nav-icon la la-bank'></i> Departments</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('doctor') }}'><i class='nav-icon la la-user-nurse'></i> Doctors</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('patient') }}'><i class='nav-icon la la-user-injured'></i> Patients</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('appointment') }}'><i class='nav-icon la la-calendar'></i> Appointments</a></li>
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blood') }}'><i class='nav-icon la la-question'></i> Bloods</a></li> --}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('humanresource') }}'><i class='nav-icon la la-address-card'></i> HumanResources</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('employee') }}'><i class='nav-icon la la-users'></i> Employees</a></li>



<li class="nav-item nav-dropdown"><a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-plus-circle"></i>Adds On</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('position') }}'><i class='nav-icon la la-map-pin'></i> Positions</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blood') }}'><i class='nav-icon la la-tint'></i> Blood</a></li>
    </ul>
{{-- for Role User Permissions --}}
<li class="nav-item nav-dropdown"><a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-gear"></i>Setting</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('role') }}'><i class='nav-icon la la-street-view'></i> Roles</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('report') }}'><i class='nav-icon la la-chart-bar'></i> Reports</a></li>
