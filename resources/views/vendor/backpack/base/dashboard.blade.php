@extends(backpack_view('blank'))

@php
// for widgets or chart
$widgets['before_content'][] = [ 'type' => 'div', 'class' => 'row', 'content' => [
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-primary mb-2',
                'value'       =>$doctor,
                'description' => 'DOCTORS',
                'progress'    => $doctor, // integer
                // 'hint'        => '8544 accounts',
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-warning mb-2',
                'value'       => $appointment,
                'description' => 'APPOINTMENTS',
                'progress'    =>  $appointment, // integer
                // 'hint'        => '567 Contacts',
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-success mb-2',
                'value'       => $patient,
                'description' => 'PATIENTS',
                'progress'    =>$patient, // integer
                // 'hint'        => '5694 Building',
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-danger mb-2',
                'value'       =>$doctor + $patient,
                'description' => 'STAFF',
                'progress'    => '8', // integer
                // 'hint'        => '1000 Lands',
            ],
        ]];
@endphp

@section('content')
{{-- for section or any method --}}
@endsection
