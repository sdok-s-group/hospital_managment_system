
<div class="container-fluid animated fadeIn">
    <div class="row pt-4">
        <div class="col-md-4 ">
            <div class="card" style="padding: 10px;">
                <br>
                <div class="box-body">
                    <div class="box-body box-profile p-0">
                        <div class="border-box text-center pb-2">
                            @if(!empty($entry->photo))
                                <img src="/{{$entry->photo}}" alt="..." class="profile-user-img img-responsive img-fluid d-block mx-auto rounded-circle img-thumbnail" style="width: 260px;height:260px;">
                            @else
                                <img src="https://pre.z1central.com/assets/default-user.png" alt="..." class="profile-user-img img-responsive img-fluid d-block mx-auto rounded-circle img-thumbnail">
                            @endif
                        </div>
                        <div class="text-center">
                            <h3 class="profile-username text-center text-capitalize text-break">{{$entry->name}}</h3>
                        </div>
                        <ul class="list-group pb-2">
                            <li class="list-group-item border-left-0 border-right-0">
                                <i class="nav-icon la la-key mr-1"></i>
                                <a href="{{$entry->code}}">{{$entry->code}}</a>
                            </li>
                            <li class="list-group-item border-left-0 border-right-0">
                                <i class="nav-icon la la-user mr-1"></i>
                                <span>{{$entry->gender}}</span>
                            </li>
                            <li class="list-group-item border-left-0 border-right-0">
                                <i class="nav-icon la la-phone mr-1"></i>
                                <a href="{{$entry->phone}}">{{$entry->phone}}</a>
                            </li>
                            <li class="list-group-item border-left-0 border-right-0">
                                <i class="nav-icon la la-envelope mr-1"></i>
                                <a href="{{$entry->email}}" class="text-break">{{$entry->email}}</a>
                            </li>
                            <li class="list-group-item border-left-0 border-right-0">
                                <strong><i class="la la-map-marker margin-r-5"></i></strong>
                                <span class="text-dark text-break">{{$entry->address}}</span>
                            </li>
                        </ul>
                        <a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" class="btn btn-primary btn-block"><b>Edit Profile</b></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="container">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-visit-tab" data-toggle="tab" href="#nav-visit" role="tab" aria-controls="nav-visit" aria-selected="true">VISIT</a>
                      <a class="nav-item nav-link" id="nav-diagnosis-tab" data-toggle="tab" href="#nav-diagnosis" role="tab" aria-controls="nav-diagnosis" aria-selected="false">DIAGNOSIS</a>
                    </div>
                  </nav>
                  <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-visit" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="table-responsive">
                            <table class="table border">
                                <thead class=" bg-light">
                                <tr class="text-primary">
                                    <th scope="col" width="20%" class="align-top text-nowrap">Appointment Date</th>
                                    <th scope="col" width="25%" class="align-top text-nowrap">Consultant </th>
                                    <th scope="col" width="14%" class="align-top text-nowrap">Problem</th>
                                    <th scope="col" width="16%" class="align-top text-nowrap">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($entry->appointments as $item)
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <tr>
                                                    <td scope="col" width="25%" class="align-top text-nowrap">{{$item->appointment_date}}</td>
                                                    <td scope="col" width="16%" class="align-top text-nowrap">{{$item->doctor->name}}</td>
                                                    <td scope="col" width="14%" class="align-top text-nowrap">{{$item->problem}}</td>
                                                    <td scope="col" width="7%" class="align-top text-nowrap">
                                                        <a href="{{ url('admin/appointment'.'/'.$entry->getKey().'/edit') }}" class="btn btn-sm btn-primary text-white"><i class="la la-pen"></i></a>
                                                        <a href="{{ url('admin/appointment'.'/'.$entry->getKey().'/show') }}" class="btn btn-sm btn-success text-white"><i class="la la-eye"></i></a>
                                                        <a href="{{ url('admin/appointment'.'/'.$entry->getKey()) }}" class="btn btn-sm btn-danger text-white"><i class="la la-trash"></i></a>
                                                    </td>
                                                </tr>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <div class="tab-pane fade" id="nav-diagnosis" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#AddDiagnosis">Add Diagnosis</button>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table border">
                            <thead class=" bg-light">
                            <tr class="text-primary">
                                <th scope="col" width="20%" class="align-top text-nowrap">Report Type</th>
                                <th scope="col" width="25%" class="align-top text-nowrap">Report Date</th>
                                <th scope="col" width="14%" class="align-top text-nowrap">Description</th>
                                <th scope="col" width="16%" class="align-top text-nowrap">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($entry->diagnosises as $item)
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <tr>
                                                <td scope="col" width="25%" class="align-top text-nowrap">{{$item->diagnosis}}</td>
                                                <td scope="col" width="16%" class="align-top text-nowrap">{{$item->report_date}}</td>
                                                <td scope="col" width="14%" class="align-top text-nowrap">{{$item->instruction}}</td>
                                                <td scope="col" width="7%" class="align-top text-nowrap">
                                                    <a href="" class="btn btn-sm btn-success text-white"><i class="la la-download"></i></a>
                                                    <a href="" class="btn btn-sm btn-danger text-white"><i class="la la-trash"></i></a>
                                                </td>
                                            </tr>
                                        </div>
                                    </div>
                                @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    {{-- add diagnosis modal --}}
                    <div class="modal fade" id="AddDiagnosis" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">Add Diagnosis</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    @include('crud::fields.text', [
                                        "field" => [
                                        "name" => "diagnosis",
                                        "label" => 'Report Type',
                                        "type"=> "text",
                                        'attributes'=>[
                                            'placeholder'=> 'Report Type',
                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-6']

                                    ]])
                                    @include('crud::fields.date',[
                                        "field"=>[
                                            "name"  =>  "report_date",
                                            "label" =>  "Report Date",
                                            "type"  =>  "date",
                                            'value' => \Carbon\Carbon::now('Asia/Phnom_Penh'),
                                            'wrapper' => ['class' => 'form-group col-md-6']
                                        ]])
                                        {{-- customize upload file --}}
                                    <div class="form-group col-md-8">
                                        <label for="file">Description</label><br>
                                        <input type="file" id="file" name="document">
                                    </div>
                                    @foreach ($entry->appointments as $item)
                                    @include('crud::fields.hidden',[
                                        "field" => [
                                        'name'   => 'appointment_id',
                                        'type'   => 'hidden',
                                        'value'  =>$item->id
                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-2']
                                    ])
                                    @endforeach
                                    @include('crud::fields.hidden',[
                                        "field" => [
                                        'name'   => 'patient_id',
                                        'type'   => 'hidden',
                                        'value'  =>  $entry->id,
                                        ],
                                        'wrapper' => ['class' => 'form-group col-md-2']
                                    ])
                                    <div class="form-group shadow-textarea col-md-12">
                                        <label for="description">Description</label>
                                        <textarea class="form-control z-depth-1" id="description" name="instruction" rows="3" placeholder="Write something here..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary" id="btn-submit" data-dismiss="modal">Save changes</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 </div>
