
<div class="d-flex">
    @foreach ($crud->entry_employee as $item)
        <div class="card mr-3 mt-3" style="width: 18rem;">
            <img src="/{{$item->photo}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Name : {{$item->name}}</h5>
                    <p class="card-text"><strong>Positon : {{$item->position->name}}</strong></p>
                    <p class="card-text">Gender : {{$item->gender}}</p>
                    <p class="card-text">Date Join : {{$item->created_at}} </p>
                    <a href="{{route('employee.show', $item->id)}}" class="btn btn-primary">Detail</a>
                </div>
        </div>
    @endforeach

    @foreach ($crud->entry_doctor as $item)
        <div class="card mr-3 mt-3" style="width: 18rem;">
            <img src="/{{$item->photo}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Name : {{$item->name}}</h5>
                    <p class="card-text"><strong>Positon :</strong></p>
                    <p class="card-text">Sepcialist : {{$item->specialist}}</p>
                    <p class="card-text">Gender : {{$item->gender}}</p>
                    <p class="card-text">Date Join : {{$item->created_at}} </p>
                    <a href="{{route('doctor.show', $item->id)}}" class="btn btn-primary">Detail</a>
                </div>
        </div>
        @endforeach

</div>

