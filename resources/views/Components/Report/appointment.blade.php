<table class="table tab-pane active col-12" id="appointment">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Date</th>
        <th scope="col">Patient ID</th>
        <th scope="col">Patient Name</th>
        <th scope="col">Gender</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Address</th>
        <th scope="col">Problem</th>
        <th scope="col">Consultant</th>
      </tr>
    </thead>
    <tbody>
    @foreach($crud->entry as $data)
        <tr>
            <th scope="row"></th>
            <td>{{$data->appointment_date}}</td>
            <td>{{$data->patient->code}}</td>
            <td>{{$data->patient->name}}</td>
            <td>{{$data->patient->gender}}</td>
            <td>{{$data->patient->phone}}</td>
            <td>{{$data->patient->address}}</td>
            <td>{!!$data->problem!!}</td>
            <td>{{$data->doctor->name}}</td>
        </tr>
    @endforeach
    </tbody>
  </table>

