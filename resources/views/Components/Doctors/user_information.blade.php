
           <div class="container-fluid animated fadeIn">
                <div class="row pt-4">
                    <div class="col-md-4 ">
                        <div class="card" style="padding: 10px;">
                            <br>
                            <div class="box-body">
                                <div class="box-body box-profile p-0">
                                    <div class="border-box text-center pb-2">
                                        @if(!empty($entry->photo))
                                            <img src="/{{$entry->photo}}" alt="..." class="profile-user-img img-responsive img-fluid d-block mx-auto rounded-circle img-thumbnail">
                                        @else
                                            <img src="/uploads/files/original/default_account.png" alt="..." class="profile-user-img img-responsive img-fluid d-block mx-auto rounded-circle img-thumbnail"  style="width:300px; height:300px">
                                        @endif
                                    </div>
                                    <div class="text-center">
                                        <h3 class="profile-username text-center text-capitalize text-break">{{$entry->name}}</h3>
                                    </div>
                                    <ul class="list-group pb-2">
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <i class="nav-icon la la-phone mr-1"></i>
                                            <a href="{{$entry->phone}}">{{$entry->phone}}</a>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <i class="nav-icon la la-envelope mr-1"></i>
                                            <a href="{{$entry->email}}" class="text-break">{{$entry->email}}</a>
                                        </li>
                                        <li class="list-group-item border-left-0 border-right-0">
                                            <strong><i class="la la-map-marker margin-r-5"></i></strong>
                                            <span class="text-dark text-break">{{$entry->address}}</span>
                                        </li>
                                    </ul>
                                    <a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" class="btn btn-primary btn-block"><b>Edit Profile</b></a>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="col-md-8">
                            <div class="profile-content card" style="padding: 20px;">
                                <div class="card-header with-border">
                                    <div class="row">
                                        <div class="col-md-8"><h5>Doctor Information</h5></div>
                                    </div>
                                </div>
                                <div class="row pl-0" style="margin-left: 30px;">
                                    <div class="col-md-6 pt-2">
                                        <label>ID : <span>{{$entry->code}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Doctor Name : <span>{{$entry->name}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Gender : <span>{{$entry->gender}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Phone : <a href="tel:{{$entry->phone}}" class="text-primary">{{$entry->phone}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Email : <a href="tel:" class="text-primary">{{$entry->email}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Department : <span>{{$entry->department_id}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Address: <span>{{$entry->address}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Specialist : <span>{{$entry->specialist}}</span></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Date Join : <a  class="text-primary">{{$entry->date_join}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Consultant Room: <a  class="text-primary">{{$entry->consultant_room}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Consultant Fee: <a  class="text-primary">{{$entry->consultant_fee}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Time in: <a  class="text-primary">{{$entry->time_in}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Time out: <a  class="text-primary">{{$entry->time_out}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Status: <a  class="text-primary">{{$entry->status}}</a></label>
                                    </div>
                                    <div class="col-md-6 pt-2">
                                        <label>Created at: <a  class="text-primary">{{$entry->created_at}}</a></label>
                                    </div>
                                </div>
                             </div>
                             <div class="card" style="paading:20px">
                                <div class="col-md-12 card-body">
                                    <div class="card-text">
                                        {{strip_tags($entry->short_biography)}}
                                    </div>
                                 </div>
                             </div>

                </div>
            </div>
