@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
@include('Components.Patients.patients_information')
@endsection


@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> --}}
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        $(document).on('show.bs.modal', '.modal', function () {
        $(this).appendTo('body');
        });
    </script>
    <script>
        $(function(){
            $('body').on('click', '#btn-submit', function(){
                    var formsubmit = new FormData();
                    var docfile = document.querySelector('#file');
                        formsubmit.append('appointment_id',$('input[name="appointment_id"]').val());
                        formsubmit.append('patient_id',$('input[name="patient_id"]').val());
                        formsubmit.append('diagnosis',$('input[name="diagnosis"]').val());
                        formsubmit.append('report_date',$('input[name="report_date"]').val());
                        formsubmit.append('instruction',$('textarea[name="instruction"]').val());
                        formsubmit.append('document',docfile.files[0]);
                axios.post('/diagnosis/store',formsubmit,{
                            headers: {
                                        'Content-Type': 'multipart/form-data'
                                    }
                }).then((response) => {
                            new Noty({
                                title: "success",
                                text: "Success add Task Activities",
                                type: "success", icon: true
                                }).show();
                                        setTimeout(function(){
                                        location.reload();
                                }, 1000);
                            console.log(response)
                        })
                    });
                });
    </script>
@endsection
