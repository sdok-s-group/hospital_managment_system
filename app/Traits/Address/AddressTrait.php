<?php

namespace App\Traits\Address;

trait AddressTrait
{
    /**
    * ADDRESS
    * @param string $parameter (city, district, commune, village, full)
    * @param string $lang (kh, en)
    * @param numeric $code (address code)
    * @return string
    */
    public static function getAddress($param, $lang, $code)
    {
        $parameters = ['city', 'district', 'commune', 'village','full'];
        $address = self::queryAddress($code);

        if(!empty($address)):

            if(!empty($lang) && in_array($lang, ['kh', 'en'])):
                $langPath = '_path_'.$lang;
                $fullPath = $address->{$langPath};
            endif;

            if(!empty($param) && in_array($param, $parameters)):
                switch($param){
                    case 'city':
                        return self::getAddressByIndex($fullPath ?? '', 0);
                    break;

                    case 'district':
                        return self::getAddressByIndex($fullPath ?? '', 1);
                    break;

                    case 'commune':
                        return self::getAddressByIndex($fullPath ?? '', 2);
                    break;

                    case 'village':
                        return self::getAddressByIndex($fullPath ?? '', 3);
                    break;

                    case 'full':
                        return self::getFullAddress($code, $fullPath ?? '', $lang);
                    break;
                }
            endif;

        endif;
        return null;

    }

    /////////////////////////////
    // GET ADDRESS BY INDEX
    /////////////////////////////
    static function getAddressByIndex($fullPath, $index)
    {
        $paths = explode('/',$fullPath);
        return !empty($paths[$index]) ? $paths[$index] : '';
    }

    /////////////////////////////
    // GET FULL ADDRESS
    /////////////////////////////
    static function getFullAddress($code, $fullPath, $lang = 'kh')
    {
        $fullAddress = '';
        $isComma = $lang == 'en' ? ',': '';
        if(!empty($fullPath) && $code):
            $paths = explode('/',$fullPath);
            $fullAddress.= !empty($paths[3]) ? ' '.self::getTypeOfAddress($code, 8, $lang).$paths[3].$isComma : '';
            $fullAddress.= !empty($paths[2]) ? ' '.self::getTypeOfAddress($code, 6, $lang).$paths[2].$isComma : '';
            $fullAddress.= !empty($paths[1]) ? ' '.self::getTypeOfAddress($code, 4, $lang).$paths[1].$isComma : '';
            $fullAddress.= !empty($paths[0]) ? ' '.$paths[0] : '';
        endif;
        return $fullAddress;
    }

    /////////////////////////////
    // GET TYPE OF ADDRESS
    /////////////////////////////
    static function getTypeOfAddress($code, $length, $lang = 'kh')
    {
        if(!empty($code) && !empty($length)):
            $code =  \Str::limit($code, $length, '');
            if(strlen($code) == $length):
                $query = self::queryAddress($code);
                if(!empty($query)):
                    $type = '_type_'.$lang;
                    return $query->{$type};
                endif;
            endif;
        endif;
        return null;
    }

    /////////////////////////////
    // QUERY ADDRESS
    /////////////////////////////
    static function queryAddress($code)
    {
        return \DB::table('kh_address')
                    ->select('_code','_path_en','_path_kh','_type_en','_type_kh')
                    ->where('_code', $code)->first();
    }
}
