<?php

namespace App\Libraries\Dashboards;

use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\Patient;

class Dashboard{
    public static function getAllDoctors(){
        $result= Doctor::count();
        return $result;
    }
    public static function getAllAppointments(){
        $result= Appointment::count();
        return $result;
    }
    public static function getAllPatients(){
        $result= Patient::count();
        return $result;
    }
}
