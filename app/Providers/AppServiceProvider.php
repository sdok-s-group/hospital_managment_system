<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Department;
use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Support\Facades\Schema;
use App\Observers\DepartmentObserver;
use App\Observers\DoctorObserver;
use App\Observers\PatientObserver;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Department::observe(DepartmentObserver::class);
        Doctor::observe(DoctorObserver::class);
        Patient::observe(PatientObserver::class);
    }
}
