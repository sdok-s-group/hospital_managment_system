<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class DoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|min:5|max:255',
            'password'   => 'required',
            'phone'      => 'required',
            'address'    => 'required',
            'photo'      => 'required',
            'specialist' => 'required',
            'consultant_room'   => 'required',
            'status'    => 'required',
            'department_id'     => 'required',
            'date_join' => 'required|date',
            'consultant_fee'    => 'required|numeric',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'department_id.required'    => 'The Department field is required ',
        ];
    }
}
