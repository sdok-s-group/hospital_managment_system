<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DoctorRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DoctorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DoctorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Doctor::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/doctor');
        CRUD::setEntityNameStrings('Add Doctor', 'Doctors');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        $this->crud->addColumn([
            'name'  => 'code',
            'label' =>  'ID',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'name',
            'label' =>  'Doctor Name',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'gender',
            'label' =>  'Gender',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'email',
            'label' =>  'Email',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'phone',
            'label' =>  'Phone',
            'type'  =>  'number',
        ]);
        $this->crud->addColumn([
            'name'  => 'specialist',
            'label' =>  'Specialist',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'label'     => 'Department',
            'type'      => 'select',
            'name'      => 'department_id',
            'entity'    => 'department',
            'attribute' => 'department_name',
            'model'     => "App\Models\Department",
        ]);
        $this->crud->addColumn([
            'name'  => 'status',
            'label' =>  'Status',
            'type'  =>  'text',
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DoctorRequest::class);
        $this->crud->setCreateContentClass('col-12');
        // CRUD::setFromDb(); // fields
            $this->crud->addField([
                'name'      => 'name',
                'label'     => 'Doctor Name',
                'type'      => 'text',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'gender',
                'label'     => 'Gender',
                'type'      => 'select2_from_array',
                'options'   => [
                            '-'         => '-',
                            'Male'      => 'Male',
                            'Female'    => 'Female',
                ],
                'default'     => 'Male',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'email',
                'label'     => 'Email',
                'type'      => 'text',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'password',
                'label'     => 'Password',
                'type'      => 'text',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'phone',
                'label'     => 'Phone',
                'type'      => 'phone',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'label'       => "Department",
                'type'        => "select2_from_ajax",
                'name'        => 'department_id',
                'entity'      => 'department',
                'attribute'   => "department_name",
                'data_source' => url("api/department"),
                'placeholder' => "Select a Department",
                'minimum_input_length'    => 0,
                'wrapper'   => ['class' => 'form-group col-md-6'],
             ]);
             $this->crud->addField([
                'label'       => "Specialist",
                'type'        => "select2_from_array",
                'name'        => 'specialist',
                'options'   => [
                    '-'         => '-',
                    'Neurology'      => 'Neurology',
                    ],
                'wrapper'   => ['class' => 'form-group col-md-6'],
             ]);

            $this->crud->addField([
                'name'      => 'date_join',
                'label'     => 'Date Join',
                'type'      => 'date ',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'consultant_room',
                'label'     => 'Consultant Room No',
                'type'      => 'number',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'consultant_fee',
                'label'     => 'Consultant Fee',
                'type'      => 'number',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'time_in',
                'label'     => 'Time in',
                'type'      => 'time',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'      => 'time_out',
                'label'     => 'Time out',
                'type'      => 'time',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);
            $this->crud->addField([
                'name'=>'address',
                'type'=>'textarea',
                'label' => 'Address',
                'wrapper'   => ['class' => 'form-group col-md-12'],
            ]);
            $this->crud->addField([
                'name' => 'short_biography',
                'type' => 'tinymce',
                'label' => 'Biography',
                'wrapper' => ['class'=> 'form-group col-md-12']
            ]);
            $this->crud->addField([
                'label'     => 'Profile Picture',
                'name'      => 'photo',
                'type'      => 'image',
                'aspect_ratio' => 1,
                'default' => 'uploads/profiles/default_account.PNG',
                'wrapperAttributes' => ['class'=> 'form-group col-md-6 col-6 bp-image-full-preview'],

            ]);
            $this->crud->addField([
                'name'      => 'status',
                'label'     => 'Status',
                'type'      => 'select2_from_array',
                'options'   => [
                    'Active'      => 'Active',
                    'Inactive'    => 'Inactive',
                    ],
                    'default'     => 'Inactive',
                'wrapper'   => ['class' => 'form-group col-md-6'],
            ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->crud->setShowView('Doctors.show');
    }
}
