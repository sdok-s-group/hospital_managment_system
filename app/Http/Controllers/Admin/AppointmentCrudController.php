<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AppointmentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Department;
/**
 * Class AppointmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppointmentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Appointment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/appointment');
        CRUD::setEntityNameStrings(' Add Appointment', 'Appointments');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'label'     => 'Patient Name',
            'type'      => 'select',
            'name'      => 'patient_id',
            'entity'    => 'patient',
            'attribute' => 'name',
            'model'     => "App\Models\Patient",
        ]);
        // $this->crud->addColumn([
        //     'label'     => 'Phone',
        //     'type'      => 'select',
        //     'name'      => 'patient_id',
        //     'entity'    => 'patient',
        //     'attribute' => 'phone',
        //     'model'     => "App\Models\Patient",
        // ]);
        $this->crud->addColumn([
            'label'     => 'Gender',
            'type'      => 'select',
            'name'      => 'patient_id',
            'entity'    => 'patient',
            'attribute' => 'gender',
            'model'     => "App\Models\Patient",
        ]);
        $this->crud->addColumn([
            'label'     => 'Doctor',
            'type'      => 'select',
            'name'      => 'doctor_id',
            'entity'    => 'doctor',
            'attribute' => 'name',
            'model'     => "App\Models\Doctor",
        ]);
        $this->crud->addColumn([
            'name'  => 'problem',
            'label' =>  'Problem',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'appointment_date',
            'label' =>  'Appointment Date',
            'type'  =>  'date',
        ]);
        $this->crud->addColumn([
            'label'     => 'Status',
            'type'      => 'select',
            'name'      => 'status_id',
            'entity'    => 'status',
            'attribute' => 'status_type',
            'model'     => "App\Models\Status",
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AppointmentRequest::class);
        $this->crud->setCreateContentClass('col-12');
        // CRUD::setFromDb(); // fields
        $this->crud->addField([
            'label'       => "Patient",
            'type'        => "select2_from_ajax",
            'name'        => 'patient_id',
            'entity'      => 'patient',
            'attribute'   => "code",
            'data_source' => url("api/patient"),
            'placeholder' => "Select a patient",
            'minimum_input_length'    => 0,
            'wrapper'   => ['class' => 'form-group col-md-6'],
         ]);
        $this->crud->addField([
            'label'       => "Status",
            'type'        => "select2_from_ajax",
            'name'        => 'status_id',
            'entity'      => 'status',
            'attribute'   => "status_type",
            'data_source' => url("api/status"),
            'placeholder' => "Select a Status",
            'minimum_input_length'    => 0,
            'wrapper'   => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'label'       => "Doctor",
            'type'        => "select2_from_ajax",
            'name'        => 'doctor_id',
            'entity'      => 'doctor',
            'attribute'   => "name",
            'data_source' => url("api/doctor"),
            'placeholder' => "Select a Doctor",
            'minimum_input_length'    => 0,
            'wrapper'   => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name'      => 'appointment_date',
            'label'     => 'Date',
            'type'      => 'date',
            'wrapper'   => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'problem',
            'label'     => 'Problem',
            'type'      => 'summernote',
            'wrapper'   => ['class' => 'form-group col-md-12'],
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
