<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReportRequest;
use App\Models\Appointment;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Report::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/report');
        CRUD::setEntityNameStrings('report', 'reports');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */

    public function index()
    {
        $appointment = Appointment::all();

        $this->crud->hasAccessOrFail('list');

        $this->crud->entry = $appointment;
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }



    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */

        // $this->crud->addColumn([
        //     'name'  => 'date',
        //     'label' =>  'Date',
        //     'type'  =>  'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'patient',
        //     'label' => 'Patien ID',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'name',
        //     'label' => 'Patient Name',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'gender',
        //     'label' => 'Label',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'phone',
        //     'label' => 'Phone Number',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'address',
        //     'label' => 'Address',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'problem',
        //     'label' => 'Problem',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'doctor_id',
        //     'label' => 'Consultant Doctor',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'charge',
        //     'label' => 'Charge',
        //     'type' => 'text',
        // ]);
        // $this->crud->addColumn([
        //     'name' => 'fee',
        //     'label' => 'Label',
        //     'type' => 'text',
        // ]);

        $this->crud->setListView('Report.show');

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReportRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
