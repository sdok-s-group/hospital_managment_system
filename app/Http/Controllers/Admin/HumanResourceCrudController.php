<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HumanResourceRequest;
use App\Models\Doctor;
use App\Models\Employee;
use App\Observers\DoctorObserver;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;
use PHPUnit\Util\Annotation\DocBlock;
use Illuminate\Support\Str;


/**
 * Class HumanResourceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HumanResourceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\HumanResource::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/humanresource');
        CRUD::setEntityNameStrings('humanresource', 'human_resources');


    }

    public function index()
    {
        $employee = Employee::all();
        $doctor = Doctor::all();

        $this->crud->hasAccessOrFail('list');


        $this->crud->entry_employee = $employee;
        $this->crud->entry_doctor = $doctor;
        
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);


        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->setListView('HumanResource.show');
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */



    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(HumanResourceRequest::class);
        $this->crud->setCreateContentClass('col-12');
        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Staff Name',
            'type' => 'text',
            'wrapper'   => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'gender',
            'label'     => 'Gender',
            'type'      => 'select2_from_array',
            'options'   => [
                        '-'         => '-',
                        'Male'      => 'Male',
                        'Female'    => 'Female',
            ],
            'default'     => 'Male',
            'wrapper'   => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name' => 'password',
            'label' => 'Password',
            'type'=> 'text',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name' => 'phone',
            'label' => 'Phone Number',
            'type' => 'text',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name' => 'position',
            'label' => 'Postion',
            'type' => 'text',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
        // $this->crud->addField([
        //     'label'       => "Department",
        //     'type    '        => "select2_from_ajax",
        //     'name'        => 'department_id',
        //     'entity'      => 'department',
        //     'attribute'   => "department_name",
        //     'data_source' => url("api/department"),
        //     'placeholder' => "Select a Department",
        //     'minimum_input_length'    => 0,
        //     'wrapper'   => ['class' => 'form-group col-md-6'],
        //  ]);
         $this->crud->addField([
            'name' => 'date_join',
            'label' => 'Date_join',
            'type' => 'date',
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);


    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}






