<?php

namespace App\Http\Controllers\Admin\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorRequest;
use App\Models\Doctor;
class DoctorController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term)
        {
            $results =Doctor::where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results =Doctor::paginate(10);
        }

        return $results;
    }
}
