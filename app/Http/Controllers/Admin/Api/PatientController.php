<?php

namespace App\Http\Controllers\Admin\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorRequest;
use App\Models\Patient;
class PatientController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term)
        {
            $results =Patient::where('code', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results =Patient::paginate(10);
        }

        return $results;
    }
}
