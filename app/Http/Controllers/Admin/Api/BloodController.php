<?php

namespace App\Http\Controllers\Admin\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Blood;

class BloodController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term)
        {
            $results =Blood::where('blood_type', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results =Blood::paginate(10);
        }

        return $results;
    }
}
