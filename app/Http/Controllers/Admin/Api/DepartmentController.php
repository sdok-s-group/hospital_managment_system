<?php

namespace App\Http\Controllers\Admin\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term)
        {
            $results =Department::where('department_name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results =Department::paginate(10);
        }

        return $results;
    }
}
