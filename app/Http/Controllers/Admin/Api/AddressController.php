<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
class AddressController extends Controller
{
    public function get($code='')
    {
        return Address::where('_code','Like',"${code}__")->orderBy('_name_en')->get()->pluck('_name_en','_code');
    }
}
