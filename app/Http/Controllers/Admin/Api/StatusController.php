<?php

namespace App\Http\Controllers\Admin\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Status;
use NunoMaduro\Collision\Adapters\Phpunit\State;

class StatusController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term)
        {
            $results =Status::where('status_type', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results =Status::paginate(10);
        }

        return $results;
    }
}
