<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PatientRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PatientCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PatientCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Patient::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/patient');
        CRUD::setEntityNameStrings('Add Patient', 'Patients');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
       // CRUD::setFromDb(); // columns
       $this->crud->addColumn([
            'name'  => 'code',
            'label' => "ID No.",
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => "Patient's name",
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'gender',
            'label' => "Gender",
            'type'  => 'text',
        ]);
        $this->crud->addColumn([
            'name'  => 'phone',
            'label' => "Phone",
            'type'  => 'number',
        ]);

        $this->crud->addColumn([
            'label'     => 'Consultant',
            'type'      => 'select',
            'name'      => 'doctor_id',
            'entity'    => 'doctor',
            'attribute' => 'name',
            'model'     => "App\Models\Doctor",
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-12');
        CRUD::setValidation(PatientRequest::class);

        // CRUD::setFromDb(); // fields
        $this->crud->addField([
            'name'  => 'name',
            'label' => "Patient's Name",
            'type'  => 'text',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'  =>  'marital_status',
            'label' =>  'Marital Status',
            'type'  =>  "select2_from_array",
            'options'     => [
                'Single' => "Single",
                'Married' => "Married",
                'In Relationship' => "In Relationship",
                'Engage' => "Engage",
            ],
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'label'       => "Doctor",
            'type'        => "select2_from_ajax",
            'name'        => 'doctor_id',
            'entity'      => 'doctor',
            'attribute'   => "name",
            'data_source' => url("api/doctors"),
            'minimum_input_length'    => 0,
            'placeholder'             => "Select a doctor",
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'label'       => "Blood",
            'type'        => "select2_from_ajax",
            'name'        => 'blood_id',
            'entity'      => 'blood',
            'attribute'   => "blood_type",
            'data_source' => url("api/blood"),
            'minimum_input_length'    => 0,
            'placeholder'             => "Select blood",
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'name'  => 'phone',
            'label' => "Phone Number",
            'type'  => 'phone',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);



        $this->crud->addField([   // radio
            'name'        => 'gender', // the name of the db column
            'label'       => 'Sex', // the input label
            'type'        => 'select2_from_array',
            'options'     => [
                'Male' => "Male",
                'Female' => "Female",
            ],
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'  =>  'email',
            'label' =>  'Email',
            'type'  =>  'text',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'  =>  'password',
            'label' =>  'Password',
            'type'  =>  'text',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'  =>  'birth_of_date',
            'label' =>  'Birth of Date',
            'type'  =>  'date',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'  =>  'address',
            'label' =>  'Address',
            'type'  =>  'text',
            'wrapper'=>['class'=>'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'label'     => 'Profile Picture',
            'name'      => 'photo',
            'type'      => 'image',
            'aspect_ratio' => 1,
            'default' => 'uploads/profiles/default_account.PNG',
            'wrapperAttributes' => ['class'=> 'form-group col-md-6 col-6 bp-image-full-preview'],

        ]);
        $this->crud->addField([
            'name'  =>  'note',
            'label' =>  'Note',
            'type'  =>  'summernote',
            'wrapper'=>['class'=>'form-group col-md-12'],
        ]);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->crud->setShowView('Patients.show');
    }
}
