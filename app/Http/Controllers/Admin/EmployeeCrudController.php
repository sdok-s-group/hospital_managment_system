<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EmployeeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use function PHPSTORM_META\type;

/**
 * Class EmployeeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmployeeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Employee::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/employee');
        CRUD::setEntityNameStrings('Add Employee', 'employees');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
        $this->crud->addColumn([
            'name'  => 'name',
            'label' =>  'Employee Name',
            'type'  =>  'text',
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Position', // Table column heading
            'type'      => 'select',
            'name'      => 'position_id', // the column that contains the ID of that connected entity;
            'entity'    => 'position', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Position", // foreign key model
         ]);
        $this->crud->addColumn([
            'name' => 'email',
            'label' => 'Email',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'address',
            'label' => 'Address',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'phone',
            'label' => 'Phone Number',
            'type' => 'text',
        ]);


    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(EmployeeRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

         $this->crud->addField([
            'name' => 'name',
            'label' => 'Employee Name',
            'wrapper' => ['class'=> 'form-group col-md-6']
         ]);
         $this->crud->addField([
            'name' => 'gender',
            'label' => 'Gender',
            'type' => 'select2_from_array',
            'options' => [
                '-' => '-',
                'Male' => 'Male',
                'Female' => 'Female',
            ],
            'default' => 'Male',
            'wrapper'   => ['class' => 'form-group col-md-6'],

         ]);
         $this->crud->addField([
            'name' => 'position_id',
            'label' => 'Position',
            'type'=> 'select2_from_ajax',
            'entity' => 'position',
            'attribute' => 'name',
            'data_source' => url("api/position"),
            'placeholder' => "Select a Position",
            'minimum_input_length'    => 0,
            'wrapper'   => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'email',
            'label'=> 'Email',
            'wrapper' => ['class' => 'form-group col-md-6']
         ]);
         $this->crud->addField([
            'name' => 'password',
            'label' => 'Password',
            'type'=> 'password',
            'wrapper'=> ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'phone',
            'label' => 'Phone Number',
            'type'=> 'phone',
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'address',
            'label' => 'Address',
            'type'=> 'text',
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'date_join',
            'label' => 'Date Join',
            'type' => 'date',
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'status',
            'label' => 'Label',
            'type' => 'select2_from_array',
            'options' => [
                'inactive' => 'inactive',
                'active' => 'active',
            ],
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'hidden',
            'type' => 'hidden',
            'wrapper' => ['class' => 'form-group col-md-6'],
         ]);
         $this->crud->addField([
            'name' => 'photo',
            'label' => 'Profile Picture',
            'type' =>  'image',
            'aspect_ratio' => 1,
            'default' => 'uploads/profiles/default_account.PNG',
            'wrapperAttributes' => ['class'=> 'form-group col-md-6 col-6 bp-image-full-preview']
         ]);
         $this->crud->addField([
            'name' => 'short_biography',
            'type' => 'tinymce',
            'label' => 'Biography',
            'wrapper' => ['class'=> 'form-group col-md-12']
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

    }
    protected function setupShowOperation (){
        $this->crud->setShowView('Employee.show');
    }
}
