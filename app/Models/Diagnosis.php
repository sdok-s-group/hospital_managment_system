<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    protected $table = 'dianosis';
    protected $primaryKey = 'id';
    public function  patients()
    {
       return $this->hasMany('App\Models\Patient','patient_id');
    }
}
