<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UploadFiles\UploadTrait;
use App\Traits\Address\AddressTrait;
use Illuminate\Support\Str;
use App\Models\Department;
use Intervention\Image\ImageManagerStatic as Image;
class Doctor extends Model
{
    use CrudTrait,UploadTrait,AddressTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'doctors';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];

    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function department(){
        return $this->belongsTo('App\Models\Department','department_id');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getFullAddressAttribute(){
        return $this->getAddress('full', 'en', $this->address);

    }
    public function setPhotoAttribute($value)
    {
        $this->attributes['photo'] = $this->base64Upload($value);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    // public function setPhotoAttribute($value)
    // {
    //     $attribute_name = "photo";
    //     $disk = config('backpack.base.root_disk_name');
    //     $destination_path = "public/uploads/profiles";
    //     if ($value==null) {
    //         \Storage::disk($disk)->delete($this->{$attribute_name});
    //         $this->attributes[$attribute_name] = null;
    //     }
    //     if (Str::startsWith($value, 'data:image'))
    //     {
    //         $image = \Image::make($value)->encode('jpg', 90);
    //         $filename = md5($value.time()).'.jpg';
    //         \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
    //         \Storage::disk($disk)->delete($this->{$attribute_name});
    //         $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
    //         $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
    //     }
    // }
    // public static function boot()
    // {
    //     parent::boot();
    //     static::deleting(function($obj) {
    //         \Storage::disk('public_folder')->delete($obj->image);
    //     });
    // }
}
