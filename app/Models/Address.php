<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $table = 'kh_address';

    public function getFullAddressAttribute()
    {
       return implode(', ',array_reverse(explode('/',$this->_path_en))) ;
    }
}
