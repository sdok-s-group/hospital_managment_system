<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('positions')->insert([
            [
                'id'                => '1',
                'name'              => 'Security',
                'description'       => 'provide midicine for patient',
                'created_at'        => now(),
            ],
            [
                'id'              => '2',
                'name'              => 'Nurse',
                'description'       => 'provide service for patient stying',
                'created_at'        => now(),

            ],
            [
                'id'              => '3',
                'name'              => 'Cleaner',
                'description'       => 'provide service for patient stying',
                'created_at'        => now(),

            ],
        ]);
    }
}
