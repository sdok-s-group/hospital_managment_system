<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\BloodSeeder;
use Database\Seeders\StatusSeeder;
use Database\Seeders\DepartmentSeeder;
use Database\Seeders\PositionSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            BloodSeeder::class,
            StatusSeeder::class,
            DepartmentSeeder::class,
            PositionSeeder::class,
        ]);
    }
}
