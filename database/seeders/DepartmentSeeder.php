<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('department')->insert([
            [
                'code'              => '0001',
                'department_name'   => 'Medical Dianosing',
                'description'       => 'provide midicine for patient',
                'status'            => 'active',
                'created_at'        => now(),
            ],
            [
                'code'              => '0002',
                'department_name'   => 'Nursing Controlling',
                'description'       => 'provide service for patient stying',
                'status'            => 'active',
                'created_at'        => now(),
            ],
        ]);
    }
}
