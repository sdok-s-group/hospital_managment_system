<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointment_status')->insert([
            [
                'status_type'   =>  'cancel',
                'color_code'    =>  '#000000',
                'created_at'    =>  now(),
            ],
            [
                'status_type'   =>  'pending',
                'color_code'    =>  '#11111',
                'created_at'    =>  now(),
            ],
            [
                'status_type'   =>  'aproved',
                'color_code'    =>  '#AB23FF',
                'created_at'    =>  now(),
            ]

        ]);
    }
}
