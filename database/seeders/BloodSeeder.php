<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
class BloodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                // We can use fatory Faker for generating data to database via seeder
                // for example
                    // $faker=Faker::create();
                    //     foreach(range(1,10) as $index){
                    //         DB::table('bloods')->insert([
                    //         'blood_type' =>$faker->Str(4),
                    //         ]);
                    // }
        DB::table('bloods')->insert([
                [
                    'blood_type'  =>'A',
                    'description' =>'Good People',
                    'created_at'  =>now(),
                ],
                [
                    'blood_type'  =>'B',
                    'description' =>'Better People',
                    'created_at'  =>now(),
                ],
                [
                    'blood_type'  =>'C',
                    'description' =>'Best People',
                    'created_at'  =>now(),
                ],
                [
                    'blood_type'  =>'O',
                    'description' =>'Cute People',
                    'created_at'  =>now(),
                ],
        ]);
    }
}
