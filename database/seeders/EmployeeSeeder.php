<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker=Faker::create();
        foreach(range(5,10) as $index){
            DB::table('employee')->insert([
            'name' =>$faker->Str(8),
            'blood_id' => $faker->numberBetween(1,4),
            'position_id' => $faker->numberBetween(1,4),
            'email' => $faker->Str(10).'@gmail.com',
            ]);
        }
    }
}
