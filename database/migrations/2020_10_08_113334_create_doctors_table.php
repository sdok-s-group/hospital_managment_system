 <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->unsignedBigInteger('department_id')->nullable();
            $table->string('name')->nullable();
            $table->text('email')->nullable();
            $table->string('password')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string ('gender')->nullable();
            $table->string('address')->nullable();
            $table->string('specialist')->nullable();
            $table->string('photo',10000)->nullable();
            $table->timestamp('date_join')->nullable();
            $table->string('consultant_room')->nullable();
            $table->integer('consultant_fee')->nullable();
            $table->time('time_in')->nullable();
            $table->time('time_out')->nullable();
            $table->string('day_work')->nullable();
            $table->string('status')->nullable();
            $table->text('short_biography', 255)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
