<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('blood_id')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('code', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('password', 100)->nullable();
            $table->string('gender', 10)->nullable();
            $table->string('photo', 100)->nullable();
            $table->string('phone', 100)->nullable();
            $table->timestamp('birth_of_date')->nullable();
            $table->string('marital_status', 100)->nullable();
            $table->string('note', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
