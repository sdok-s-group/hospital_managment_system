<?php

use App\Http\Controllers\Admin\Api\BloodController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Api\DepartmentController;
use App\Http\Controllers\Admin\Api\DoctorController;
use App\Http\Controllers\Admin\Api\PatientController;
use App\Http\Controllers\Admin\Api\StatusController;
use App\Http\Controllers\Admin\Api\PositionController;
use App\Http\Controllers\DiagnosisController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("/api/department", [DepartmentController::class,'index']);
Route::get("/api/status", [StatusController::class,'index']);
Route::get("/api/doctor", [DoctorController::class,'index']);
Route::get('/', function () {
    return view('welcome');
});
Route::get("/api/doctors", [DoctorController::class,'index']);
Route::get("/api/blood", [BloodController::class,'index']);
Route::get("/api/patient", [PatientController::class,'index']);
Route::get("/api/position", [PositionController::class,'index']);
Route::post('/diagnosis/store',[DiagnosisController::class,'store']);

