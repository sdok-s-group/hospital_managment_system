<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.


Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::get('dashboard','DashboardController@index');
    Route::get('address/{code?}', 'Api\AddressController@get')->name('address.get');
    Route::crud('department', 'DepartmentCrudController');
    Route::crud('doctor', 'DoctorCrudController');
    Route::crud('patient', 'PatientCrudController');
    Route::crud('appointment', 'AppointmentCrudController');
    Route::crud('blood', 'BloodCrudController');
    Route::crud('humanresource', 'HumanResourceCrudController');
    Route::crud('address', 'AddressCrudController');
    Route::crud('status', 'StatusCrudController');


    Route::crud('employee', 'EmployeeCrudController');
    Route::crud('position', 'PositionCrudController');
    Route::crud('role', 'RoleCrudController');
    Route::crud('report', 'ReportCrudController');
});